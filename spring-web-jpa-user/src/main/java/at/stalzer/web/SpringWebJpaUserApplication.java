package at.stalzer.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import at.stalzer.web.db.User;
import at.stalzer.web.db.UserService;

@SpringBootApplication
public class SpringWebJpaUserApplication{ //implements CommandLineRunner{

    public static void main(String[] args) {
        SpringApplication.run(SpringWebJpaUserApplication.class, args);
    }
//    @Autowired
//    private UserService userService;
//    
//	@Override
//	public void run(String... arg0) throws Exception {
//		Iterable<User> it = userService.findAll();
//		for (User user : it) {
//			System.out.println(user);
//		}
//		
//	}
}
