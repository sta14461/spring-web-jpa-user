package at.stalzer.web.db;

import org.springframework.data.repository.CrudRepository;


public interface UserRepository extends  CrudRepository<User, Integer>{
	
}

	