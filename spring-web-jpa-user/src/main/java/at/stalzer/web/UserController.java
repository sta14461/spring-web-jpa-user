package at.stalzer.web;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import at.stalzer.web.db.User;
import at.stalzer.web.db.UserService;

@Controller
public class UserController 
{
	private final UserService userService;

	@Autowired
	public UserController(UserService userService)
	{
		this.userService=userService;
	}


//	@RequestMapping(value="/users")
//	public ModelAndView getUsers()
//	{
//		ModelAndView mav=new ModelAndView("users");
//
//		Object[] liste={"abc","ef"};
//
//		mav.addObject("users",liste);
//		return mav;
//	}
	
	@RequestMapping(value="/users")
	public ModelAndView getUsers()
	{
		ModelAndView mav = new ModelAndView("users");
		ArrayList<User> liste2 = new ArrayList<User>();
		Iterable<User> it = userService.findAll();

		for (User user : it) 
		{
			liste2.add(user);
		}

		mav.addObject("users", liste2);

		return mav;
	}
}
